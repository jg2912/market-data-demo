// rabbitmqConnectExample.cpp : Defines the entry point for the console application.
//

#include <SimpleAmqpClient\SimpleAmqpClient.h>

#include <iostream>
#include <stdlib.h>

using namespace AmqpClient;
int main()
{

	Channel::ptr_t channel;
	channel = Channel::Create("45.55.213.149", 5672,"simnow", "bZ4eqB_Puo", "/");

	channel->DeclareQueue("jiongqueue");
	channel->BindQueue("jiongqueue", "amq.direct", "jiongkey");

	BasicMessage::ptr_t msg_in = BasicMessage::Create();

	msg_in->Body("This is a small message.");

	channel->BasicPublish("amq.direct", "alankey", msg_in);

	channel->BasicConsume("jiongqueue", "consumertag");

	BasicMessage::ptr_t msg_out = channel->BasicConsumeMessage("consumertag")->Message();

	std::cout << "Message text: " << msg_out->Body() << std::endl;

}

// These configs fail to connect
//
//"45.55.213.149", 5672, "jiong", "FWYVcBoLMC"		
//"45.55.213.149", 5672, "", ""
//"45.55.213.149", 5672, "simnow", "bZ4eqB_Puo"
//"45.55.213.149", 8088, "simnow", "bZ4eqB_Puo"
//"45.55.213.149", 8088, "", ""
