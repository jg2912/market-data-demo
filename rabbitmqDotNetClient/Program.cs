﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace rabbitmqDotNetFirstExample
{
    class Program
    {
        static void Main(string[] args)
        {
            string u = "simnow";
            string pwd = "bZ4eqB_Puo";
            string vhost = "/";
            string hostname = "45.55.213.149";
            int port = 8088;
            string exchangeName = "marketdata";
            string routingKey = "";

            Solution s = new Solution(u, pwd, vhost, hostname, port);
            IConnection conn = s.createConnection();

            IModel channel = conn.CreateModel();
            channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);

            DataSource datas = new DataSource();
            foreach(string piece in datas.sources)
            {
                byte[] msgBodyBytes = System.Text.Encoding.UTF8.GetBytes(piece);
                channel.BasicPublish(exchangeName, routingKey, null, msgBodyBytes);
                Console.WriteLine(string.Format("send to exchange {0} message: {1}", exchangeName, piece));
            }
            

            Console.WriteLine("Press any key to close...");
            Console.ReadKey();
            channel.Close(200, "bye");
            conn.Close();
        }
    }

    public class Solution
    {
        private string user { get; set; }
        private string pwd { get; set; }
        private string vhost { get; set; }
        private string hostname { get; set; }
        private int port { get; set; }
        private ConnectionFactory factory; 
        public Solution(string u, string p, string vh, string h, int pt)
        {
            factory = new ConnectionFactory();
            user = u;
            pwd = p;
            vhost = vh;
            hostname = h;
            port = pt;
        }

        public IConnection createConnection( )
        {   
            factory.UserName = user;
            factory.Password = pwd;
            factory.VirtualHost = vhost;
            factory.HostName = hostname;
            factory.Port = port;

            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }

    public class DataSource
    {
        public string [] sources { get; set; }

        public DataSource()
        {
            sources = new string[] { "Contract|ExchangeID|TradingDate|LastModifiedTime|UpdateMillisec|LastPrice|Volume|Turnover|OpenInterest|ClosePrice",
                                    "IH1509||20150911|09:20:02|0|2191|333|2.18921e+08|14770|1.79769e+308",
                                    "ag1512||20150911|10:39:09|500|3340|900978|4.51089e+10|396244|1.79769e+308",
                                    "IH1509||20150911|11:29:25|0|2193|3341|2.20371e+09|13649|1.79769e+308",
                                    "IC1509||20150911|14:08:34|500|6310.2|4453|5.69772e+09|8781|1.79769e+308" };
        }
    }
}
