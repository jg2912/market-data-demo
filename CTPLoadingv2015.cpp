// CTPLoadingv2015.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ThostFtdcMdApi.h"
#include "ThostFtdcTraderApi.h"
#include "ThostFtdcUserApiDataType.h"
#include "ThostFtdcUserApiStruct.h"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <mutex>

std::mutex mtx; 

class ThostFtdcMdApiWrapper : public CThostFtdcMdSpi
{
public:
	void OnFrontConnected()
	{
		std::cout << "Connected\n";

		CThostFtdcReqUserLoginField reqField;
		memset(&reqField, 0, sizeof(reqField));
		TThostFtdcBrokerIDType brokerId = { '9','9', '9', '9' };
		TThostFtdcUserIDType userId = { 'x','i','a','o','l','e','n' };
		TThostFtdcPasswordType pwd = { 'B','a','c','h','e','l','i','e','r' };
		strcpy_s(reqField.BrokerID, brokerId);
		strcpy_s(reqField.UserID, userId);
		strcpy_s(reqField.Password, pwd);

		int reqId = 0;
		int ret = mdP->ReqUserLogin(&reqField, reqId++);
		std::cout << "Return value of request user login is " << ret << "\n";
		std::cout << "0 for good, -1 for network, -2 for too many queued requests, -3 for too many requests in 1s\n";
	}

	void OnFrontDisconnected(int nReason)
	{
		std::cout << "Disconnected due to reason " << nReason << "\n";
	}

	void OnHeartBeatWarning(int nTimeLapse)
	{
		std::cout << nTimeLapse << "s passed without heart beating...\n";
	};

	void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "User response comes back\n";
		if (pRspInfo->ErrorID == 0)
		{
			std::cout << "Login succeeded.\n";
			char* IF = "IF1509";
			char* IH = "IH1509";
			char* IC = "IC1509";
			char* AG = "ag1512";
			char* NI = "ni1601";
			char* CP = "RM1601";
			char* OPT = "10000032";
			char* contracts[] = {IF, AG, CP, OPT,IH, IC, NI};
			int ret = mdP->SubscribeMarketData(contracts, 7);
			std::cout << "Return value of subscription is " << ret << "\n";
		}
		else
		{
			std::cout << "Login failed.\n";
		}
	};

	void OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "User log out request gets responded.\n";
	};

	void OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "Error reported";
	};

	void OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "Response comes back for market data subscription.\n";
		std::cout << "Return message is " << pRspInfo->ErrorMsg << "\n";
	};

	void OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "Response comes back for unsubscription of market data\n";
	};

	void OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "Response comes back for subscription of quotes stack\n";
	};

	void OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
	{
		std::cout << "Response comes back for unsubscription of quotes stack\n";
	};

	void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
	{
		std::cout << "Market data coming for " << pDepthMarketData->InstrumentID << "...\n";

		mtx.lock();
		std::string fileName = "../deposit/DataTemp_"  +std::to_string(fileCount++);
		mtx.unlock();

		std::ofstream myFile;
		myFile.open(fileName);
		myFile << "Contract|ExchangeID|TradingDate|LastModifiedTime|UpdateMillisec|LastPrice|Volume|Turnover|OpenInterest|ClosePrice\n";
		myFile << pDepthMarketData->InstrumentID << "|" << pDepthMarketData->ExchangeID << "|" << pDepthMarketData->TradingDay << "|" << pDepthMarketData->UpdateTime << "|" 
				<< pDepthMarketData->UpdateMillisec << "|" << pDepthMarketData->LastPrice << "|" << pDepthMarketData->Volume << "|" << pDepthMarketData->Turnover << "|" 
				<< pDepthMarketData->OpenInterest << "|" << pDepthMarketData->ClosePrice;
		myFile.close();

	};

	void OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp)
	{
		std::cout << "Quotes coming...\n";
	};

	ThostFtdcMdApiWrapper(CThostFtdcMdApi* p)
	{
		mdP = p;
	};
private:
	CThostFtdcMdApi* mdP;
	static int fileCount;
};

int ThostFtdcMdApiWrapper::fileCount = 0;

int main()
{
	char frontAddress_string[] = "tcp://180.168.146.187:10010";
	char *frontAddress = frontAddress_string;

	CThostFtdcMdApi* pUserApi = CThostFtdcMdApi::CreateFtdcMdApi();
	ThostFtdcMdApiWrapper* pUserSpi = new ThostFtdcMdApiWrapper(pUserApi);
	pUserApi->RegisterSpi(pUserSpi);
	pUserApi->RegisterFront(frontAddress);
	pUserApi->Init();
	std::cout << "Main thread begin to join...\n";
	pUserApi->Join();
	std::cout << "Session finihsed.\n";
	std::cin.get();

    return 0;
}

